import re
import csv
from jinja2 import Template


csv_head = ['Action','Protocol','SourceIPSubnet','Subnet_mask','Destination','Subnet_mask','PortsRanges'] 
res_interim = []


def concat_port_range(rules_l):
        out_l = []
        out_res = []
        for ln in rules_l:
                if "eq" in ln and "any" not in ln:
                        #i_eq = ln.index('eq')
                        ln = list(filter(lambda a: a != 'eq',ln))
                        ln = ln[:len(ln)-1]
                        out_l.append(ln)
                elif "range" in ln and "any" not in ln:
                        i_rang = ln.index("range")
                        ln[i_rang] = ln[i_rang+1]+'-'+ln[i_rang+2]
                        ln = ln[:len(ln)-2]
                        out_l.append(ln)

        for ln in rules_l:
                if "any" in ln:
                        #print("Here" + str(len(ln)))
                        ln = list(filter(lambda a: a != 'eq',ln))
                        ln = ln[:len(ln)-2]
                        out_l.append(ln)
                
        return out_l
                        

def rule_types_extr(pos_s_any, pos_d_any, pos_src_host, pos_dest_host, pos_dest_nw, pos_source_nw, r_list):
        s_any =[]
        d_any = []
        s_d_host = []
        s_host_d_nw = []
        s_d_nw = []
        s_nw_d_host = []
        
        for ln in r_list:
                if "object-group" in ln:     ### Remove lines with 'object-group' in them
                        ln = list(filter(lambda a: a != 'object-group',ln))
                elif ln[pos_s_any] == "any":                                      ### Source any list # pos_s_any = 2  
                        ln.insert(pos_s_any + 1,"")
                        if ln[4] == "host":
                                ln = list(filter(lambda a: a != 'host',ln))
                                ln.insert(pos_s_any + 3,"")
                        s_any.append(ln)
                        
                elif ln[pos_d_any] == "any":                                    ### Dest any   #pos_d_any = 4    
                        ln.insert(pos_d_any + 1,"")
                        d_any.append(ln)

                elif ln[pos_src_host] == "host" and ln[pos_dest_host] == "host":  ### Source host - Dest host  #pos_src_host = 2  pos_dest_host = 4  
                        ln.insert(pos_src_host + 2,"")
                        ln.insert(pos_dest_host + 3,"")
                        ln = list(filter(lambda a: a != 'host',ln))
                        s_d_host.append(ln)

                elif ln[pos_src_host] == "host" and re.match('^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$',ln[pos_dest_nw]):  ### Source host - Dest netw   #pos_src_host = 2  pos_dest_nw = 4   
                        s_host_d_nw.append(ln)


                elif re.match('^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$',ln[pos_source_nw]) and re.match('^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$',ln[pos_dest_nw]):  ### Source netw - Dest netw  #pos_source_nw = 2  pos_dest_nw = 4 
                        s_d_nw.append(ln)

                elif re.match('^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$',ln[pos_source_nw]) and ln[pos_dest_host] == "host":  ### Source netw - Dest host    #pos_source_nw = 2  pos_dest_host = 4 
                        ln = list(filter(lambda a: a != 'host',ln))
                        ln.insert(pos_dest_host + 1,"")
                        s_nw_d_host.append(ln)
        
        
        return concat_port_range(s_any), concat_port_range(d_any), concat_port_range(s_d_host), concat_port_range(s_host_d_nw), concat_port_range(s_d_nw), concat_port_range(s_nw_d_host)


'''
###  WIP  ####
def cisco_to_GCP_jinja(list_type, list_inp):
        src_ranges_tcp = set()
        src_ranges_udp = set()
        src_ranges_icmp = set()
        src_ranges_icmp6 = set()
        src_ranges_ip = set()

        ports_ranges_tcp = set()
        ports_ranges_udp = set()

        action = ['allowed','denied']
        
        for ln in list_inp:
                if list_type == "sr_any":
                        src_ranges_tcp.add('0.0.0.0/0')
                        src_ranges_udp.add('0.0.0.0/0')
                        src_ranges_icmp.add('0.0.0.0/0')
                        src_ranges_icmp6.add('0.0.0.0/0')
                        src_ranges_ip.add('0.0.0.0/0')

                        if ln[1] == 'tcp':
                                ports_ranges_tcp.add(ln[6])
                        if ln[1] == 'udp':
                                ports_ranges_udp.add(ln[6])
                 
        
        src_ranges_tcp_ls = list(src_ranges_tcp)
        src_ranges_udp_ls = list(src_ranges_udp)
        src_ranges_icmp_ls = list(src_ranges_icmp)
        src_ranges_icmp6_ls = list(src_ranges_icmp6)
        src_ranges_ip_ls = list(src_ranges_ip)
        ports_ranges_tcp_ls = list(ports_ranges_tcp)
        ports_ranges_udp_ls = list(ports_ranges_udp)
        
        with open('dm_fw_template.jinja') as file_:
            templ = Template(file_.read())
        print(templ.render( src_ranges_tcp=src_ranges_tcp_ls, src_ranges_udp=src_ranges_udp_ls, ports_ranges_tcp=ports_ranges_tcp_ls, ports_ranges_udp=ports_ranges_udp_ls ))
        

### END WIP  ####
'''


with open('firewall_rules.txt','r') as f_content:
        with open('firewall.csv','w') as output:
                writer = csv.writer(output, lineterminator='\n')
                writer.writerow(csv_head)
                for line in f_content:   
                        s_line = line.lstrip()
                        #x = re.findall(r'(^access-list.*permit+\s*tcp+\s*[0-9].*|^access-list.*deny.*[0-9].*")', s_line)
                        # match space [\S\n\t]    (*.)(host|any|.*[0-9])
                        #x = re.findall(r'((^access-list)(.*)(permit|deny)(\s)(tcp|udp|icmp|icmp6|ip)(\s)(host|any|(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}))(\s)(host|any|(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}))(\s)(host|any|(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}))(eq) )', s_line)
                        x = re.findall(r'((^access-list)(.*)(permit|deny)(\s)(tcp|udp|icmp|icmp6|ip)(.*))', s_line)
                        if len(x) != 0:
                                y = re.split(r'\s',str(x[0]))
                                z=y[5:14]
                                res_interim.append(z)
                               # print(z)
                             # writer.writerow(z)   # write to CSV file


src_any, dest_any, src_dest_host, src_host_dest_nw, src_dest_nw, src_nw_dest_host = rule_types_extr(2,4,2,4,4,2,res_interim)


print("############################### Source any #############################################!!!!")
print(*src_any, sep="\n")

print("############################### Dest any #############################################")
print(*dest_any, sep="\n")

print("############################### Source host - Dest host #############################################!!!!")
print(*src_dest_host, sep="\n")

print("############################### Source host - Dest netw #############################################!!!!")
print(*src_host_dest_nw, sep="\n")


print("############################### Source netw - Dest netw #############################################!!!!")
print(*src_dest_nw, sep="\n")


print("############################### Source netw - Dest host #############################################!!!!")
print(*src_nw_dest_host, sep="\n")

with open('firewall.csv','w') as output:
        writer = csv.writer(output, lineterminator='\n')
        writer.writerow(csv_head)
        for line in src_any:
                writer.writerow(line)   # write to CSV file
        for line in dest_any:
                writer.writerow(line)   # write to CSV file
        for line in src_dest_host:
                writer.writerow(line)   # write to CSV file
        for line in src_host_dest_nw:
                writer.writerow(line)   # write to CSV file
        for line in src_dest_nw:
                writer.writerow(line)   # write to CSV file
        for line in src_nw_dest_host:
                writer.writerow(line)   # write to CSV file

# Define main() function
# def main():

# Execute `main()` function 
# if __name__ == '__main__':
#    main()
