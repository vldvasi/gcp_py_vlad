import re
import csv
import itertools
from jinja2 import Template
import ipaddress
import pandas as pd
from collections import defaultdict


obj_grp = []
host_subl = []
sub_any = []
csv_head = ['Action','Protocol','SourceIP_Network','SubnetMask','DestinationIP_Network','SubnetMask','Port_PortRange'] 
res_interim = []
result = []
                        
def rem_eq_range(rules_l):
# remove the unnecessary strings like 'eq', 'range', concat ports ranges and trim the lists
        ip_subl = []
        eq_subl = []
        range_subl = []
        end_subl = []
        out_l = []
        
        for ln in rules_l:
                if ln[1] == "ip" or ln[1] == "icmp" or ln[1] == "icmp6":
                        ln = ln[0:6]
                        ln.insert(6,"")
                        ln.insert(7,"")
                ip_subl.append(ln)
                
        for ln in ip_subl:    
                if ln[6] == "eq":
                        ln.pop(6)
                        #ln = list(filter(lambda a: a != 'eq',ln))
                        #ln = ln[:len(ln)-1]
                eq_subl.append(ln)
                
        for ln in eq_subl:
                if ln[6] == "range":
                        i_rang = ln.index("range")
                        ln[i_rang] = ln[i_rang+1]+'-'+ln[i_rang+2]
                        ln = ln[:len(ln)-2]
                range_subl.append(ln)

        for elem in range_subl:
                if len(elem) > 6:
                        end_subl = elem[:7]
                else:
                        end_subl = elem
                out_l.append(end_subl)

        return out_l


def rem_list_contains(txt_cont,lol):
# removes a list that contains element like 'object-group' form list of lists
        obj_grp = []
        obj_grp_tmp = []
        
        for sub_i, ln in enumerate(lol):
                if str(txt_cont) in ln:
                        obj_grp_tmp.append(ln)
                        
        for elem in lol:
                if elem not in obj_grp_tmp:
                        obj_grp.append(elem)
        return obj_grp


def rem_string_list(txt_cont,lol):
# Remove an element like 'host' from a list of lists and add an empty string element for subnetmask position
        host_subl = []
        for ln in lol:              
                indices = [i for i, x in enumerate(ln) if x == str(txt_cont)]
                if len(indices) > 0:
                        for j in indices:
                                ln[j] = ln[j+1]
                                ln[j+1] = ""
                host_subl.append(ln)
        return host_subl


def repl_string_list(el_find,el_repl,lol):
# Inserts empty string elements in the list of lists after a specific element like 'any'
        any_subl = []
        for ln in lol:
                indices = [i for i, x in enumerate(ln) if x == str(el_find)]
                if len(indices) > 0:
                        for j in indices:
                                ln[j] = str(el_repl)
                                ln.insert(j + 1,"")                                     
                any_subl.append(ln)
        return any_subl


def imp_file_filter(f_path,start_l,end_l):
# Import file content and apply regex to get only fw rules from the raw config
        f_list = []
        with open(str(f_path),'r') as f_content:
                for line in f_content:   
                        s_line = line.lstrip()
                        x = re.findall(r'((^access-list)(.*)(permit|deny)(\s)(tcp|udp|icmp|icmp6|ip)(.*))', s_line)
                        if len(x) != 0:
                                y = re.split(r'\s',str(x[0]))
                                z=y[int(start_l):int(end_l)]
                                f_list.append(z)
                                #print(z)
        return f_list


def exp_csv(f_csv, lol):
# Export list of lists to csv file 
        with open(f_csv,'w') as output:
                writer = csv.writer(output, lineterminator='\n')
                writer.writerow(csv_head)
                for line in lol:
                        writer.writerow(line)   # write to CSV file  


def repl_nw_prefix(pos_s_ip,pos_d_ip,lol):
# replace subnet mask with network prefix
        s_nw_pref = []
        sd_nw_pref = []

        for elem in lol:
                if elem[pos_s_ip+1] == "" and elem[pos_s_ip] != "0.0.0.0/0":
                        elem[pos_s_ip] = elem[pos_s_ip]+"/32"
                elif elem[pos_s_ip+1] != "" and elem[pos_s_ip] != "0.0.0.0/0":
                        ipadd = elem[pos_s_ip]+"/"+elem[pos_s_ip+1]
                        elem[pos_s_ip] = ipaddress.ip_network(ipadd)
                s_nw_pref.append(elem)

        for elem in s_nw_pref:
                if elem[pos_d_ip+1] == "" and elem[pos_d_ip] != "0.0.0.0/0":
                        elem[pos_d_ip] = elem[pos_d_ip]+"/32"
                elif elem[pos_d_ip+1] != "" and elem[pos_d_ip] != "0.0.0.0/0":
                        ipadd = elem[pos_d_ip]+"/"+elem[pos_d_ip+1]
                        elem[pos_d_ip] = ipaddress.ip_network(ipadd)
                sd_nw_pref.append(elem)
        
        return sd_nw_pref
      

def rules_to_dict(lol, prot, action, ip_src_dest, ip_filt):
# add rules from lists to dict.
        df_head = ['Action','Protocol','SourceIP_Network','SubnetMask','DestinationIP_Network','SubnetMask','Port_PortRange']   

        df = pd.DataFrame(lol, columns=df_head)
        rule_filter = df[(df["Protocol"] == prot) & (df["Action"] == action) & (df[str(ip_src_dest)] == ip_filt)]      # HERE add search IP in Subnet function
        
        rule_columns = rule_filter[[ip_src_dest,"Port_PortRange"]]
        rule_array_tupl = list(rule_columns.itertuples(index=False, name=None))  # Convert DataF to array of Tuples

        rule_dict = defaultdict(set)   # Convert Array of Tuples into Dict. with unique keys 
        for k, v in rule_array_tupl:
                rule_dict[k].add(v)

        rule_dict_list = {}    # Convert to Dict of lists to comply with Jinja lists of ports
        for i, j in rule_dict.items():
                rule_dict_list[i]= list(j)
        
        return rule_dict_list

###  WIP  ####
def rules_to_GCP_jinja(filt_action,lol):
# parse dictionary with fw rules and create GCP Deployment M. Jinja file
        df_head = ['Action','Protocol','SourceIP_Network','SubnetMask','DestinationIP_Network','SubnetMask','Port_PortRange']   

        df = pd.DataFrame(lol, columns=df_head)

        # print("HERE START")
        # print(lol)    # HERE testing only
        # print("HERE END")

        ip_allow_d = rules_to_dict(lol,"ip",filt_action,"DestinationIP_Network", "10.80.4.176/32")
        tcp_allow_d = rules_to_dict(lol,"tcp",filt_action,"DestinationIP_Network", "10.80.4.10/32")
        udp_allow_d = rules_to_dict(lol,"udp",filt_action,"DestinationIP_Network", "10.80.4.10/32")
        icmp_allow_d = rules_to_dict(lol,"icmp",filt_action,"SourceIP_Network", "10.80.4.10/32")
        icmp6_allow_d = rules_to_dict(lol,"icmp6",filt_action,"SourceIP_Network", "10.80.4.10/32")

                
        #tcp_allow = df[(df["Protocol"] == "tcp") & (df["Action"] == "permit")]
        #tcp_allow_col = tcp_allow[["SourceIP_Network","Port_PortRange"]]
        #tcp_allow_l = list(tcp_allow_col.itertuples(index=False, name=None))  # Convert DataF to array of Tuples

        #tcp_allow_d = defaultdict(set)   # Convert Array of Tuples into Dict. with unique key
        #for k, v in tcp_allow_l:
        #        tcp_allow_d[k].add(v)
        #print(tcp_allow_d['0.0.0.0/0'])
        #print(tcp_allow_d)
        
        with open('dm_fw_template.jinja') as file_:
            templ = Template(file_.read())
        print(templ.render(tcp_allow_d=tcp_allow_d, udp_allow_d=udp_allow_d, ip_allow_d=ip_allow_d, icmp_allow_d=icmp_allow_d, icmp6_allow_d=icmp6_allow_d ))
        outpt = templ.render(tcp_allow_d=tcp_allow_d, udp_allow_d=udp_allow_d, ip_allow_d=ip_allow_d, icmp_allow_d=icmp_allow_d, icmp6_allow_d=icmp6_allow_d )

        with open('firewall.jinja', 'w') as f:
                f.write(outpt)

### END WIP  ####


# Define main() function
def main():
        res_interim = imp_file_filter("firewall_rules.txt",5,14)
        obj_grp = rem_list_contains("object-group",res_interim)
        host_subl = rem_string_list("host",obj_grp)
        sub_any = repl_string_list("any","0.0.0.0/0",host_subl)
        result = rem_eq_range(sub_any)

        # remove duplicates and create a single list of lists
        #result.extend(src_any)
        result.sort()
        result_no_dup = list(result for result,_ in itertools.groupby(result))
        
        exp_csv("cisco_firewall.csv", result_no_dup)
        #rules_to_GCP_jinja("permit", repl_nw_prefix(2,4,result_no_dup))
        rules_to_GCP_jinja("permit", repl_nw_prefix(2,4,result_no_dup))
        
        
# Execute `main()` function 
if __name__ == '__main__':
        main()
