import re
import csv
import itertools
from jinja2 import Template
import ipaddress
import pandas as pd
from collections import defaultdict

obj_grp = []
host_subl = []
sub_any = []
csv_head = ['Action','Protocol','SourceIP_Network','SubnetMask','DestinationIP_Network','SubnetMask','Port_PortRange'] 
res_interim = []
result = []

def get_editnext_block_jun(f_path):
# reads file and outputs a dict of lists with the rules within the edit-next blocks 
        with open(str(f_path),'r') as f_jun:
                rules = {}
                cn = itertools.count()           
                for k, v in itertools.groupby(f_jun, lambda x: not x.lstrip().startswith('edit') and not x.lstrip().startswith('next')):
                        if k:
                                rules[next(cn)] = list(map(str.strip, v))
        
        rules.pop(0)            # removes first element as it contains no rules
        rules.pop(len(rules))   # removes last element as it contains no rules
        # for x, y in rules.items():
        #         print(x, y)
        return rules

def extr_rlines_jun(dofl):
# extracts from dict of lists the lines containing the str_search strings. Outputs dict of list of lists.
        str_search = ['action','service','srcaddr','dstaddr','service','comments']
        r_dofll = {}
        cn = itertools.count()
        for x, y in dofl.items():
                lofl = []
                for line in y:
                        ln=line.split(" ",2)     # Split by first 2 whitespace only
                        #print(ln)
                        if any(el in str_search for el in ln):      
                                lofl.append(ln)
                r_dofll[next(cn)] = lofl
        #print(r_dofll)
        return r_dofll

def extr_rules_jun(dofll):
# extract from a dict of lists of lists the fw rules elements described in variable rule_header
        r_lofl = []
        inter_lst = []
        rule_header = ['action','service','srcaddr','dstaddr','service','comments']

        for x, y in dofll.items():
                for lofl in y:
                        if any(i in rule_header for i in lofl): 
                                inter_lst.append(lofl[2])             # get the 3rd element from the list like [set, service, "3306_TCP" "3307_TCP"]     
                if len(inter_lst) < 5:                                # add 'NA' elements to lists that are not same lenght
                        inter_lst.append('NA')
                #r_lofl.append(inter_lst)
                r_lofl.append(permute_list_jun(inter_lst))
                inter_lst = []                                     
        print(r_lofl)
        return r_lofl

def permute_list_jun(lst):
# oder the list's elements to follow the rule_header 
        r_lst = []
        rule_header_order = [2, 3, 0, 1, 4]
        r_lst = [lst[i] for i in rule_header_order]
        #print(r_lst)
        return r_lst


# Define main() function
def main():
        rules_dict_jun = get_editnext_block_jun("ISM-VDOM-policies-juniper.txt")
        rules_lines = extr_rlines_jun(rules_dict_jun)
        extr_rules_jun(rules_lines)
        
# Execute `main()` function 
if __name__ == '__main__':
        main()
