# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact



import re
import os.path
import csv

csv_head = ['Action','Prot','Src_ip','Src_subnet_m','Dest_ip','Dest_subnet_m']
result = []

with open('firewall_rules.txt','r') as f_content:
        with open('firewall.csv','w') as output:
                writer = csv.writer(output, lineterminator='\n')
                writer.writerow(csv_head)
                for line in f_content: #iterate through every line
                        x = re.findall('access-list.*permit ip [0-9].*|access-list.*deny ip [0-9].*', line)
                        if len(x) != 0:
                                y = re.split(r'\s',str(x[0]))
                                z=y[5:11]
                                print(z)
                                writer.writerow(z)
