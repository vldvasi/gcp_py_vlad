#from lib.helper import Helper
def GenerateConfig(context):
    """Generates config."""
    
    icmp_name = 'delete-default-allow-icmp'
    internal_name = 'delete-default-allow-internal'
    rdp_name = 'delete-default-allow-rdp'
    ssh_name = 'delete-default-allow-ssh'
    #dependency = 'icmpdefaultrule'
    
    res = [
    {
		'name': icmp_name,
        'action': 'gcp-types/compute-beta:compute.firewalls.delete',
        'metadata':
         {
         	'runtimePolicy': ['CREATE']
         },	
        'properties':
        {
            	'firewall': 'default-allow-icmp'
        }			
	}, 
	
	{
		'name': internal_name,
        'action': 'gcp-types/compute-beta:compute.firewalls.delete',
        'metadata':
         {
         	'runtimePolicy': ['CREATE']
         },	
        'properties':
        {
            	'firewall': 'default-allow-internal'
        }			
	}, 

    {
		'name': rdp_name,
        'action': 'gcp-types/compute-beta:compute.firewalls.delete',
        'metadata':
         {
         	'runtimePolicy': ['CREATE']
         },	
        'properties':
        {
            	'firewall': 'default-allow-rdp'
        }			
	}, 	
	
    {
		'name': ssh_name,
        'action': 'gcp-types/compute-beta:compute.firewalls.delete',
        'metadata':
         {
         	'runtimePolicy': ['CREATE']
         },	
        'properties':
        {
            	'firewall': 'default-allow-ssh'
        }			
	}, 
 	
	
	{
        'name': 'list_default_network',
        'action': 'gcp-types/compute-v1:compute.networks.delete',
        'metadata':
           {
         	  'runtimePolicy': ['CREATE'],
         	  'dependsOn': [ icmp_name, internal_name, rdp_name, rdp_name ]
           },
        'properties': 
        {
            'network': "default"
        }
    }]

    return { 'resources':  res }